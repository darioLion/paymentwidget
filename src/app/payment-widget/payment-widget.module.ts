import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { PaymentWidgetComponent } from './components/payment-widget/payment-widget.component';
import { paymentWidgetContainers } from './containers/index';
import { CountrySelectorService } from './services/country-selector.service';
import { HttpClientModule } from  '@angular/common/http';
import { MatButtonModule } from '@angular/material';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { DataService } from './services/data-service.service';

@NgModule({
  declarations: [
    PaymentWidgetComponent,
    ...paymentWidgetContainers
  ],
  imports: [
    CommonModule,
    SharedModule,
    HttpClientModule,
    MatButtonModule,
    MatRadioModule,
    FormsModule,
    MatSelectModule
  ],
  exports: [
    PaymentWidgetComponent,
    ...paymentWidgetContainers
  ],
  providers: [
  	CountrySelectorService,
    DataService
  ]
})
export class PaymentWidgetModule { }
