import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Country } from '../../shared/interfaces/country';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  currentCountryDataSource = new BehaviorSubject<Country>({name: '', code: ''});
  public currentCountry = this.currentCountryDataSource.asObservable();

  constructor() { }

  changeCurrentCountry(country: Country) {
    this.currentCountryDataSource.next(country);
  }
}
