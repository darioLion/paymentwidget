import { TestBed } from '@angular/core/testing';

import { CountrySelectorService } from './country-selector.service';

describe('CountrySelectorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CountrySelectorService = TestBed.get(CountrySelectorService);
    expect(service).toBeTruthy();
  });
});
