import { Injectable } from '@angular/core';
import { Country } from '../../shared/interfaces/country';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountrySelectorService {
  currentCountry: string;
  endPoint = 'https://api.ipgeolocation.io/ipgeo';

  constructor(private httpClient: HttpClient) {}

  getCurrentCountry(): Observable<any>{
  	const params = new HttpParams().set('apiKey','87fd94a50fc14d71b9296eca80ff36c4');
  	return this.httpClient.request('GET', this.endPoint, {responseType:'json', params});
  }

  setCurrentCountry(country: Country): void{
    this.currentCountry = country.code;
  }

  getAllCountries(): Observable<any> {
  	return this.httpClient.get('./assets/json/names.json');
  }
}
