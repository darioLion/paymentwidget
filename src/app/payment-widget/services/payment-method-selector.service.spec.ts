import { TestBed } from '@angular/core/testing';

import { PaymentMethodSelectorService } from './payment-method-selector.service';

describe('PaymentMethodSelectorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentMethodSelectorService = TestBed.get(PaymentMethodSelectorService);
    expect(service).toBeTruthy();
  });
});
