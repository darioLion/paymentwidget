import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaymentMethodSelectorService {
  endPoint = 'https://api.paymentwall.com/api/payment-systems/';

  constructor(private httpClient: HttpClient) {}

  getPaymentMethods(currentCountry): Observable<any> {
  	const params = new HttpParams().set('country_code', currentCountry.code ).set('key','a1c9b51e35c089107de9fa6f815ec34e');
  	return this.httpClient.request('GET', this.endPoint, {responseType:'json', params});
  }
}
