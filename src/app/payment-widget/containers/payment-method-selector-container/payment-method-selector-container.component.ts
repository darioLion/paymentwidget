import { Component, OnInit } from '@angular/core';
import { PaymentMethod } from '../../../shared/interfaces/payment-method';
import { PaymentMethodSelectorService } from '../../services/payment-method-selector.service';
import { DataService } from '../../services/data-service.service';
import { Country } from '../../../shared/interfaces/country';

@Component({
  selector: 'app-payment-method-selector-container',
  templateUrl: './payment-method-selector-container.component.html',
  styleUrls: ['./payment-method-selector-container.component.css']
})
export class PaymentMethodSelectorContainerComponent implements OnInit {
  paymentMethods: PaymentMethod[];
  activePaymentMethod: PaymentMethod;
  currentCountry: Country;

  constructor(private paymentMethodSelectorService: PaymentMethodSelectorService,
              private dataService: DataService) { }

  ngOnInit() {
    this.dataService.currentCountry.subscribe(
      data =>{ this.currentCountry = data;
        this.paymentMethodSelectorService.getPaymentMethods(this.currentCountry).subscribe(
      		methods => this.paymentMethods = !methods ? [] : methods,
      		error => console.log(error)
      	);
      }
    );
  }

}
