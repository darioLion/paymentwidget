import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentMethodSelectorContainerComponent } from './payment-method-selector-container.component';

describe('PaymentMethodSelectorContainerComponent', () => {
  let component: PaymentMethodSelectorContainerComponent;
  let fixture: ComponentFixture<PaymentMethodSelectorContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentMethodSelectorContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentMethodSelectorContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
