import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentMethodFormContainerComponent } from './payment-method-form-container.component';

describe('PaymentMethodFormContainerComponent', () => {
  let component: PaymentMethodFormContainerComponent;
  let fixture: ComponentFixture<PaymentMethodFormContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentMethodFormContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentMethodFormContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
