import { Component, OnInit, Input } from '@angular/core';
import { CountrySelectorService } from '../../services/country-selector.service';
import { Country } from '../../../shared/interfaces/country';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-country-selector-container',
  templateUrl: './country-selector-container.component.html',
  styleUrls: ['./country-selector-container.component.css']
})
export class CountrySelectorContainerComponent implements OnInit {
  currentCountry: Country;
  countriesList: Country[] = [];

  constructor(private countrySelectorService: CountrySelectorService) { }

  ngOnInit() {
    this.countrySelectorService.getCurrentCountry().subscribe(
  	  data => this.currentCountry = {'code': data.country_code2, 'name': data.country_name},
  	  error => console.log(error)
  	);
  
  	this.countrySelectorService.getAllCountries().subscribe(
  	  data => this.countriesList = data,
  	  error => console.log(error));  	
  }

}
