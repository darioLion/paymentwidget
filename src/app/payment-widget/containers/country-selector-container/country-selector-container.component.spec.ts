import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountrySelectorContainerComponent } from './country-selector-container.component';

describe('CountrySelectorContainerComponent', () => {
  let component: CountrySelectorContainerComponent;
  let fixture: ComponentFixture<CountrySelectorContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountrySelectorContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountrySelectorContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
