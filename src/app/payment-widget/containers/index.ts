import { CountrySelectorContainerComponent } from './country-selector-container/country-selector-container.component';
import { PaymentMethodFormContainerComponent } from './payment-method-form-container/payment-method-form-container.component';
import { PaymentMethodSelectorContainerComponent } from './payment-method-selector-container/payment-method-selector-container.component';

export const paymentWidgetContainers = [
  CountrySelectorContainerComponent,
  PaymentMethodFormContainerComponent,
  PaymentMethodSelectorContainerComponent
];