export interface PaymentMethod {
  id: string;
  name: string;
  new_window: boolean;
  img_url: string;
  pricepoints?: PricePoint[];
}

export interface PricePoint {
	amount: number;
	currency: string;
	currency_converted: string;
	amount_converted: number;
}