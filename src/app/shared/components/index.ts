import { CountrySelectorComponent } from './country-selector/country-selector.component';
import { PaymentMethodFormComponent } from './payment-method-form/payment-method-form.component';
import { PaymentMethodSelectorComponent } from './payment-method-selector/payment-method-selector.component';

export const sharedComponents = [
  CountrySelectorComponent,
  PaymentMethodFormComponent,
  PaymentMethodSelectorComponent
];