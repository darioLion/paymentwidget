import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-payment-method-form',
  templateUrl: './payment-method-form.component.html',
  styleUrls: ['./payment-method-form.component.css']
})
export class PaymentMethodFormComponent implements OnInit {
  minDate: Date;
  paymentForm: any;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
  	this.paymentForm = this.fb.group({
  		cardHolder: ['', Validators.pattern('[a-zA-Z ]*')],
  		cardNumber: ['', Validators.pattern('[0-9 ]*')],
  		expDate: '',
  		cvv: ['', Validators.pattern('[0-9 ]*')],
  	});
  	this.minDate = new Date();;
  }

  submitForm(){

  }
}
