import { Component, OnInit, Input } from '@angular/core';
import { PaymentMethod } from '../../interfaces/payment-method';

@Component({
  selector: 'app-payment-method-selector',
  templateUrl: './payment-method-selector.component.html',
  styleUrls: ['./payment-method-selector.component.css']
})
export class PaymentMethodSelectorComponent implements OnInit {
  @Input() paymentMethod: PaymentMethod;

  constructor() { }

  ngOnInit() {
  }

}
