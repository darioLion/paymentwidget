import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { of } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { Country } from  '../../interfaces/country';
import { DataService } from '../../../payment-widget/services/data-service.service';

@Component({
  selector: 'app-country-selector',
  templateUrl: './country-selector.component.html',
  styleUrls: ['./country-selector.component.css']
})
export class CountrySelectorComponent implements OnInit {
  countryControl = new FormControl();
  countryName: string;

  private _behaviourSubjectCountry = new BehaviorSubject<Country>(undefined);

  @Input() countriesList;
  @Input()
  set currentCountry(value) {
  	this._behaviourSubjectCountry.next(value);
  };

  get currentCountry() {
  	return this._behaviourSubjectCountry.getValue();
  }

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this._behaviourSubjectCountry.subscribe(
      data => {
    	if (data) {
    	  this.countryControl.setValue(this.currentCountry.name);
  		}
      }
    );
  }

  changeCurrentCountry(e){
    this.currentCountry = e.option.value;
    this.dataService.changeCurrentCountry(this.currentCountry);
  }

}
